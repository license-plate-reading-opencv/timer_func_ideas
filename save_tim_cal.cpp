#include <iostream>
#include <chrono>
#include <map>
#include <iomanip>
#include <cstdio> // For getchar() function on Unix-like systems

int main() {
    char key;
    std::chrono::system_clock::time_point start_time;
    std::map<char, std::chrono::system_clock::time_point> key_timings;

    std::cout << "Press any key to start/stop timing for that key. Press 'q' to quit." << std::endl;

    while (true) {
        key = getchar(); // Get the pressed key

        if (key == 'q') {
            // Quit the program if 'q' is pressed
            break;
        } else if(key != '\n') {
            // Check if the key is already being timed
            if (key_timings.find(key) == key_timings.end()) {
                // Start timing for the key if it's not being timed already
                start_time = std::chrono::system_clock::now();
                key_timings[key] = start_time;
                std::cout << "Timing started for key '" << key << "' at ";
                std::time_t start_time_c = std::chrono::system_clock::to_time_t(start_time);
                std::cout << std::put_time(std::localtime(&start_time_c), "%T") << std::endl;
            } else {
                // Stop timing for the key and calculate the time difference
                auto end_time = std::chrono::system_clock::now();
                auto duration = end_time - key_timings[key];
                auto hours = std::chrono::duration_cast<std::chrono::hours>(duration).count();
                auto minutes = std::chrono::duration_cast<std::chrono::minutes>(duration).count() % 60;
                auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration).count() % 60;
                auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() % 1000;
                std::cout << "Timing stopped for key '" << key << "' at ";
                std::time_t end_time_c = std::chrono::system_clock::to_time_t(end_time);
                std::cout << std::put_time(std::localtime(&end_time_c), "%T") << std::endl;
                std::cout << "Time difference: " 
                          << std::setw(2) << std::setfill('0') << hours << ":"
                          << std::setw(2) << std::setfill('0') << minutes << ":"
                          << std::setw(2) << std::setfill('0') << seconds << ":"
                          << std::setw(3) << std::setfill('0') << milliseconds << std::endl;
                // Remove the key from the map to stop timing it
                key_timings.erase(key);
            }
        }
    }

    return 0;
}
