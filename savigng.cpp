#include <iostream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <cstdio> // For getchar() function on Unix-like systems

int main() {
    char key;
    std::chrono::system_clock::time_point start_time;

    std::cout << "Press 'q' to save the current time." << std::endl;

    while (true) {
        key = getchar(); // Get the pressed key

        if (key == 'q') {
            // Get the current time
            start_time = std::chrono::system_clock::now();
            std::time_t now_c = std::chrono::system_clock::to_time_t(start_time);

            // Convert the current time to a tm struct
            std::tm* now_tm = std::localtime(&now_c);

            // Print the current time in the desired format
            std::cout << "Time saved: " << std::put_time(now_tm, "%H:%M:%S:") << std::chrono::duration_cast<std::chrono::milliseconds>(start_time.time_since_epoch()).count() % 1000 << std::endl;
        }
    }

    return 0;
}

