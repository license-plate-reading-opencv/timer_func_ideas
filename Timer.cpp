#include <iostream>
#include <chrono>
#include <thread>
#include <iomanip>

void displayTimer(std::chrono::time_point<std::chrono::steady_clock> start_time) {
    while (true) {
        auto current_time = std::chrono::steady_clock::now();
        auto elapsed = current_time - start_time;

        // Convert elapsed time to hours, minutes, seconds, and milliseconds
        auto total_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
        
  
        auto hours = total_milliseconds / 3600000;
        auto minutes = (total_milliseconds % 3600000) / 60000;
        auto seconds = (total_milliseconds % 60000) / 1000;
  	auto milliseconds = total_milliseconds % 1000;
  	
        // Print the elapsed time in the format "hours:minutes:seconds:milliseconds"
        std::cout << std::setfill('0') << std::setw(2) << hours << ":"
                  << std::setfill('0') << std::setw(2) << minutes << ":"
                  << std::setfill('0') << std::setw(2) << seconds << ":"
                  << std::setfill('0') << std::setw(2) << milliseconds << "\r";
        std::cout.flush();
	
        // Wait for 1 millisecond before updating the timer display
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

    }
}

int main() {
    std::chrono::time_point<std::chrono::steady_clock> start_time = std::chrono::steady_clock::now();
    displayTimer(start_time); // Start the timer display

    return 0;
}

